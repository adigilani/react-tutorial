import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Homepage from '../components/homepage';
import AnotherFancyPage from '../components/newPage'
export default (
  <Switch>
    <Route exact path="/" component={Homepage} />
    <Route path="/main" component={Homepage} />
    <Route path="/anotherpage" component={AnotherFancyPage} />
  </Switch>
);

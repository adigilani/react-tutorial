/* eslint-disable react/no-unused-state */
import React, { Component } from 'react';

import AppContext from './AppContext';

class AppContextProvider extends Component {
  state = {
    loaderShowing: false
  };

  setStateAsync(state) {
    return new Promise(resolve => {
      this.setState(state, resolve);
    });
  }

  render() {
    const { loaderShowing } = this.state;
    const { children } = this.props;
    return (
        <AppContext.Provider
            value={{
                loaderShowing,
                toggleLoader: async status => {
                await this.setStateAsync({
                  loaderShowing: status
                });
            }
        }}
      >
        {children}
      </AppContext.Provider>
    );
  }
}

export default AppContextProvider;

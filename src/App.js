import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';

import AppUtils from './utils';

import './index.css';

import CustomHeader from './components/header';
import CustomFooter from './components/footer';
import routes from './routes/routes';
import Loader from './components/loader';

function App() {
  return (
    <AppUtils.AppContextProvider>
      <Loader></Loader>
      <div className="header">
        <CustomHeader></CustomHeader>
      </div>
      <div className="content">
        <Router>{routes}</Router>
      </div>
      <div className="footer">
        <CustomFooter ></CustomFooter>
      </div>
    </AppUtils.AppContextProvider>
  );
}

export default App;

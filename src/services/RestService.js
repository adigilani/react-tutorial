import axios from 'axios';

class RestService {

    getPhotosWithKeyWord = (pageSize = 1, per_page = 10, query = "javascript") => {
        const url = `https://api.unsplash.com/search/photos?page=${pageSize}&per_page=${per_page}&query=${query}`;
        const headers = this.getHeaders();
        return axios.get(url, {headers})
    }

    getRandomPhotos = () => {
        const url = `https://api.unsplash.com/photos/random`;
        const headers = this.getHeaders();
        return axios.get(url, {headers})
    }

    getHeaders = () => {
        const authToken = "90f92671f7d4e4a609ad0d292b94a13414a4a62ff5ef8a012d8bb009d58256c7";
        const headers = {"Content-Type": "multipart/form-data"};
        headers["Authorization"] = `Client-ID ${authToken}`;
        headers["Access-Control-Allow-Origin"] = '*';
        return headers;
    }

    getDate = () => {
        const url = "http://worldtimeapi.org/api/ip";
        return axios.get(url);
    }

}

export default RestService;

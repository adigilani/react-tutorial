import React from 'react';
import FooterPresentationWithView from '../presentation/footerpresentation';

function footerContainerWithBusinessLogic(props){

    return (
        <div>
            <FooterPresentationWithView>A Quick App to Show React Development</FooterPresentationWithView>
        </div>
    );

}

export default footerContainerWithBusinessLogic;
import React, {Component} from 'react';

class footerPresentationWithView extends Component {
    render() {
        const { children } = this.props;
        return (
            <div>
                <div>{children}</div>
            </div>
        );
    };
}

export default footerPresentationWithView;
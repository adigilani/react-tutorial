import React from 'react';
import HeaderPresentationWithView from '../presentation/headerpresentation';

function headerContainerWithBusinessLogic(){

    return (
        <HeaderPresentationWithView></HeaderPresentationWithView>
    );

}

export default headerContainerWithBusinessLogic;
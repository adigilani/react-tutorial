import React, {Component}  from 'react';
import Nav from 'react-bootstrap/Nav'

class headerPresentationWithView  extends Component {

    render(){

        return (
            <Nav activeKey="/home">
                <Nav.Item>
                    <Nav.Link href="/">Search</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                    <Nav.Link href="/anotherpage">Random</Nav.Link>
                </Nav.Item>
            </Nav>
        );
    }
}

export default headerPresentationWithView;
import React, {useEffect} from "react";
import useCustomDate from '../../../hooks'


export default function NewPagePresentational(props) {
    const [date, getDate] = useCustomDate();
    useEffect(()=>{
        if (!date) {
            getDate();
        }
    });

    return date ? (
        <div>
            <button onClick={getDate}>{date.datetime}</button>
            <p></p>
            <img src={props.image} alt="mypic"></img>
        </div>
    ) : null;
};

import React, {useState, useEffect} from 'react';
import RestService from '../../../services';
import PresentationLayer from '../presentation/newpagepresentation';

export default function NewPageContainer() {

    const [imageData, setImageData] = useState({image: null});
    const {image} = imageData;

    useEffect((image) => {

        if (!image) {
            new RestService().getRandomPhotos().then((response)=>{
                setImageData({image: response.data.urls.regular});
            });
        }
    }, [])


    return (
        <PresentationLayer image={image}></PresentationLayer>
    );

};

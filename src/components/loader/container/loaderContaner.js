import React, { Component } from 'react';

import AppUtils from '../../../utils';

import LoaderPresentation from '../presentation/loaderPresentation';

class LoaderContainer extends Component {
    
    static contextType = AppUtils.AppContext;

    render(){
        const { loaderShowing } = this.context;
        return(
            <LoaderPresentation show={loaderShowing}></LoaderPresentation>
        );
    }
}

export default LoaderContainer;
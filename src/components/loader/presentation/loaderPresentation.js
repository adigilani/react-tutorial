import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './loaderPresentationStyle.css';

class LoaderPresentation extends Component {

    PropTypes = {
        show: PropTypes.bool.isRequired
    };

    render(){
        const { show } = this.props;
        return(
            show && <div class="loading">Loading&#8230;</div>
        );
    }
}

export default LoaderPresentation;
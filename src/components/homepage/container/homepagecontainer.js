import React, { Component } from 'react';

import HomePagePresentationWithView from '../presentation/homepagepresentation';

import AppUtils from '../../../utils';
import RestService from '../../../services';

class homepageContainerWithBusinessLogic extends Component{
    static contextType = AppUtils.AppContext;

    constructor() {
        super();
        this.restService = new RestService();
        this.state = {
            results: null,
            total_pages: 0,
            total: 0
        };
    }

    setStateAsync(state) {
        return new Promise(resolve => {
          this.setState(state, resolve);
        });
    }
    
    doSearch = async (page = 1, pagesize = 5, query) => {
        const { toggleLoader } = this.context;
        toggleLoader(true);
        console.log(`searching for ${query}`);
        try {
            const response = await this.restService.getPhotosWithKeyWord(page, pagesize, query);
            await this.setStateAsync({
                ...response.data
            });
            toggleLoader(false);
            console.log(this.state);
        } catch (e) {
            toggleLoader(false);
            console.log(e);
        }
    }

    render() {
        const {
            results,
            total_pages,
            total
        } = this.state;
        return (
            <div>
                <HomePagePresentationWithView 
                    commenceSearchCallBack={this.doSearch}
                    results={results}
                    total_pages={total_pages}
                    total={total}
                ></HomePagePresentationWithView>
            </div>
        );
    }

}

export default homepageContainerWithBusinessLogic;
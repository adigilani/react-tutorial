import React, {Component} from 'react';
import PropTypes from 'prop-types';

import Card from 'react-bootstrap/Card'
import Button from 'react-bootstrap/Button'
import Container from 'react-bootstrap/Container'
import FormControl from 'react-bootstrap/FormControl'
import InputGroup from 'react-bootstrap/InputGroup'

class homepagePresentationWithView extends Component {

    PropTypes = {
        commenceSearchCallBack: PropTypes.func.isRequired,
        results: PropTypes.object.isRequired,
        total_pages: PropTypes.number.isRequired,
        total: PropTypes.number.isRequired,
    };

    constructor(props) {
        super(props);
        this.state = {
            query: ''
        };
    }

    handleChange(event) {
        let fleldVal = event.target.value;
        this.setState({query: fleldVal});
        console.log(fleldVal);
    }

    doSearch(){
        const { commenceSearchCallBack } = this.props;
        const { query } = this.state;
        commenceSearchCallBack(1, 10, query);
    }

    renderImages(results) {
        const imageCards = [];
        results.forEach(image => {
            imageCards.push(
                <Card style={{ width: '18rem', margin: '5px'}}>
                    <Card.Img variant="top" src={image.urls.small} />
                    <Card.Body>
                        <Card.Text>{image.description}</Card.Text>
                        <Button variant="primary" onClick={(event)=>{alert('I am not gonna do all the work');}}>View</Button>
                    </Card.Body>
                </Card>
            )
        });
        return (
            <Container>
                {imageCards}
            </Container>
        );
    }

    render() {
        const { results, total_pages, total } = this.props;
        return (
            <div className="homepage">
                 <InputGroup className="mb-3">
                    <FormControl
                        placeholder="Image Query"
                        aria-label="Image Query"
                        aria-describedby="imgquery"
                        onChange={this.handleChange.bind(this)}
                    />
                    <InputGroup.Append>
                        <Button variant="outline-secondary" onClick={this.doSearch.bind(this)}>Search</Button>
                    </InputGroup.Append>
                </InputGroup>
                {results && this.renderImages(results)}
            </div>
        );
    };
}

export default homepagePresentationWithView;
import {useState} from 'react';

import RestService from '../services';

export default function useCustomDate() {

    const [date, setDate] = useState(null);

    const fetchServerDate = async () => {
        const response = await new RestService().getDate();
        setDate({...response.data});
    };

    // the return part which is the date variable here
    return [date, fetchServerDate];
};